import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function FeaturedProducts() {
    return (
        <div className='d-flex flex-column my-5 banner-bg p-5 text-white text-center'>
        <h2>Check our product</h2>
        <Row className="my-3 text-dark h-100">
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height" >
                <Card.Img  variant="top" src="https://scontent.fmnl33-2.fna.fbcdn.net/v/t1.15752-9/346135515_3160116137625275_5087151877097827524_n.png?_nc_cat=104&ccb=1-7&_nc_sid=ae9488&_nc_ohc=EGHAXQ9VsZQAX81VVKV&_nc_ht=scontent.fmnl33-2.fna&oh=03_AdT5PlMOBH4CP-D8JBr9vyDtCkXaD5YhHQOfS9x9jDHKbw&oe=6492E64E" />
                <Card.Header>
                <Card.Title>TXT Collection</Card.Title>
                </Card.Header>
               {/* <Card.Body>
                    <Card.Text>
                    Explore all-new personalization features, privacy and security enhancements, and more ways to communicate seamlessly.
                    </Card.Text>
                </Card.Body>*/}
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://scontent.fmnl33-2.fna.fbcdn.net/v/t1.15752-9/346144617_574110577931655_4331083953568586228_n.png?_nc_cat=105&ccb=1-7&_nc_sid=ae9488&_nc_ohc=172Pvzi5_fAAX-3eGBv&_nc_ht=scontent.fmnl33-2.fna&oh=03_AdSE-v0buGpWaeqB2HxlwqkVy2C2qrC8aMaidDbl5d9XWw&oe=6492CFF6" />
                <Card.Header>
                    <Card.Title>TWICE Collection</Card.Title>
                    </Card.Header>
                {/*<Card.Body>
                    <Card.Text>
                    Buy with discounts the cutting-edge technology of Samsung's mobile phones, home electronics, shop Galaxy watches and earbuds at the best price.
                    </Card.Text>
                </Card.Body>*/}
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://scontent.fmnl33-4.fna.fbcdn.net/v/t1.15752-9/346132744_1604912260012871_3220032445857426197_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=SzQ7rBGDpEEAX85lgEH&_nc_ht=scontent.fmnl33-4.fna&oh=03_AdQWxd79OrnX0-Mr7hCAOjvgCaqUsMFbnah54nNhb-_xmg&oe=6492E5D0" />
                <Card.Header>
                    <Card.Title>JIMIN Collection</Card.Title>
                    </Card.Header>
                {/*<Card.Body>
                    <Card.Text>
                    View latest OPPO phone with high camera quality, trendy design and advanced specifications. A mobile phone brand designs innovative mobile photography & video technology.
                    </Card.Text>
                </Card.Body>*/}
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://scontent.fmnl33-2.fna.fbcdn.net/v/t1.15752-9/346153427_970063617763841_381664761671215218_n.png?_nc_cat=105&ccb=1-7&_nc_sid=ae9488&_nc_ohc=b1dnpoJDWr8AX9H1AhE&_nc_ht=scontent.fmnl33-2.fna&oh=03_AdTKRMnIe0-7uHd5ReC5lFLi90xuIupwN9wyhUOjsXS7pw&oe=6492CBCC" />
                <Card.Header>
                    <Card.Title>J-HOPE Collection</Card.Title>
                    </Card.Header>
                {/*<Card.Body>
                    <Card.Text>
                As a top-three global smartphone company and a leading consumer AIoT platform, Xiaomi is fully committed to leveraging its scale and efficiency to create a sustainable economy for our users, employees, partners and the planet.
                    </Card.Text>
                </Card.Body>*/}
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
        </Row>
        </div>
    );
}
