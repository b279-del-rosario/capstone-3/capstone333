import {Carousel, Image} from 'react-bootstrap';

export default function HomeCarousel() {
return (
    <div class="top-content" >
    <Carousel className='vh-75 w-100 d-inline-block mb-5 shadow' fade>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit  carousel-height carousel-caption-bg"
        src="https://scontent.fmnl33-1.fna.fbcdn.net/v/t1.15752-9/346146881_233216992739287_8192144216523798370_n.png?stp=dst-png_s2048x2048&_nc_cat=108&ccb=1-7&_nc_sid=ae9488&_nc_ohc=348PDYef8YUAX9EbK7p&_nc_ht=scontent.fmnl33-1.fna&oh=03_AdT4XZPkGqGA2lLTJ3eVPP-pNGVkMsFOF9E0jywq99oWag&oe=649284FD"
        alt="First slide"
        />
        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25 '>
       {/* <div  className='carousel-text'>
        <h1>Gift yourself a new tech!</h1>
        <p>Buy the cheapest tech products in TechTrends using your computer or smartphone</p>
        </div>*/}
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src="https://scontent.fmnl33-2.fna.fbcdn.net/v/t1.15752-9/346142417_931982551345741_1524536105350487671_n.png?stp=dst-png_s2048x2048&_nc_cat=105&ccb=1-7&_nc_sid=ae9488&_nc_ohc=mdezJW37qbUAX93hMqK&_nc_ht=scontent.fmnl33-2.fna&oh=03_AdQJT99GuJfAIwM3kNcTsliuS0NtHzZeO-6EpOjKXoFlHw&oe=6492A633"
        alt="Second slide"
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        {/*<div  className='carousel-text'>
        <h1>Life is incredible with new technology</h1>
        <p>Get yourself updated with the latest and cheapest smart devices in the market</p>
        </div>*/}
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src="https://scontent.fmnl33-3.fna.fbcdn.net/v/t1.15752-9/346151381_776042090697905_4695437895656179145_n.png?stp=dst-png_s2048x2048&_nc_cat=110&ccb=1-7&_nc_sid=ae9488&_nc_ohc=mnHdvdXoFXEAX-lQRTn&_nc_ht=scontent.fmnl33-3.fna&oh=03_AdQmy2LRbkqaXBxB5wt0o3g1wVe65xTDXlWvOY1DMvW0FA&oe=6492931D"
        alt="Third slide"
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        {/*<div  className='carousel-text'>
        <h1>Ran out of cash?</h1>
        <p>
            Don't worry, we got you. Use your E-wallet instead!
        </p>
        </div>*/}
        </Carousel.Caption>
    </Carousel.Item>
    </Carousel>
    </div>
);
}
