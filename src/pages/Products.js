import { useEffect, useState, useContext } from "react";
import { Navigate, useParams, Link } from "react-router-dom";
import HomeBanner from "../components/HomeBanner";
// import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";
import logo from "../images/logo.jpeg"
import {Row, Col, Card, Button, Container, Modal} from "react-bootstrap"

function ProductCard({product}){
	const [addModal, setAddModal] = useState(false);
	const [quantity, setQuantity] = useState(0)

/*	 if (data) {
                    Swal.fire({
                        title: "PRODUCT SOLD!",
                        icon: "success",
                        text: `Thank you for purchasing ${productName}.
                        A total of Php ${price} was deducted to your E-wallet.`
                    })
                    navigate("/products");
                } else {
                    Swal.fire({
                        title: "Oops! Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }*/

	return <>
		 <Card className="m-2">
				<Card.Body className="text-center">
					<Card.Title>{product.name}</Card.Title>
					<Card.Subtitle>{product.description}</Card.Subtitle>
					<Card.Subtitle>Price: ₱{product.price}</Card.Subtitle>
					<div className="d-flex justify-content-between mb-3 p-2">
						<Button onClick={() => {
							if (quantity > 0 ) {
								setQuantity(quantity - 1)
							}
						}}>
							-
						</Button>
						{quantity}
						<Button onClick={() => {
								setQuantity(quantity + 1)
						}}>
							+
						</Button>
					</div>
					<Button variant="primary" onClick={()=>setAddModal(true)}>Buy Now!</Button>
				</Card.Body>	
			</Card>
			<Modal size="lg" aria-labelledby="contained-modal-title-vcenter" centered show={addModal}>
				<Modal.Header className="banner-bg text-light d-flex justify-content-between">
					<Modal.Title>Product Sold!</Modal.Title>
					<Button onClick={()=>setAddModal(false)}>X</Button>
				</Modal.Header>
				<Modal.Body>
					<div>
					<p>Thank you for purchasing {product.name}.
                        A total of ₱{product.price} was deducted to your balance</p>
					</div>

				</Modal.Body>
			</Modal>
			</>
}

export default function Products() {

	const data = {
		title: "IdolFindsPH",
		content: "",
		destination: "/products",
		label: "Shop Now!",
		image: {logo}
	}

	const { user } = useContext(UserContext);


	const [products, setProducts] = useState([]);
	const { quantityId } = useParams();

	useEffect(() =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data);
		})
	}, []);


	return(
		(user.isAdmin)
		?
			<Navigate to="/dashboard" />
		:	
		<>
	        <div className="p-5 text-center">
				<HomeBanner bannerProp={data}/>
				<h1>Our Products</h1>
				<div className="mx-auto">
				{products ? (<div className="" xs={12} md={6} lg={4}>
						{products.map(product => (
						<ProductCard product={product} key={product._id}/>
						))}
					</div>):(<div>loading</div>)}
					
				</div>
	   		</div>
	   		
		</>
	)
}